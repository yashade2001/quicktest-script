# QuickTest Script
This script compiles SystemUI, Settings, SettingsProvider, Framework (frameworks-res.apk and framework.jar) and pushes the output files to the device using ADB. We usually just compile SystemUI to test our code but sometimes you need to make modifications in Settings or Frameworks for your code. For example when you add your own class(es) in FWB (outside of SystemUI) you need to build framework.jar and push that to your device too. This takes some time. But this script will do all the things automatically. Also it can backup SystemUI.apk, Settings.apk, SettingsProvider.apk framework.jar and frameworks-res.apk before pushing new ones to your phone (Useful when you get SystemUI FC :P).

# Usage
Copy the script to the root of your Android source directory and do this: chmod a+x quicktest.sh. When you execute the script it just compiles the SystemUI, Settings, SettingsProvider.apk and Framework (frameworks-res.apk and framework.jar). It doesn't push files to the device by default. If you want to compile and push output files to your device use -p (or --push) parameter:
./quicktest.sh -p (it will reboot your device after pushing files)
Note that you don't need to clean out directory manually with "make clean", just add -c (or --clean) parameter. If you want to backup current files in your phone before pushing use -b (or --backup) parameter. It will backup SystemUI.apk, Settings.apk, SettingsProvider.apk framework.jar and frameworks-res.apk to ~/quicktest-backups directory.

# Example Usage
./quicktest.sh -c -p -b
(clean out directory, compile, backup, push)

# Embedding to the Android Tree
You can create a function in build/envsetup.sh called "quicktest" and paste the script in that function. Don't forget to remove unneded codes to initialize source tree. Or just wait me :P I'll do that soon.

# Notes
- Don't specify -jN. It gets the cpu thread number from /proc/cpuinfo.
- If you use -b parameter, it will delete previous backups first.

# TODO List
- A parameter to specify a module and build (for example: Gallery2).
